import { Injectable } from '@angular/core';

@Injectable()
export class StringConcat {
  capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

  depluralize(string) {
      return string.slice(0, string.length - 1);
  }
}
