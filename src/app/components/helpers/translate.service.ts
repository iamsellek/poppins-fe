import { Injectable } from '@angular/core';

@Injectable()
export class Translate {
  translateActivityType(string) {
    if (string === 'milkings') {
      string = 'breastfeedings & pumpings';
    } else if (string === 'milking') {
      string = 'one';
    }

    return string;
  }
}
