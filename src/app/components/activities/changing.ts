export class Changing {
    id: number;
    hasPoo: boolean;
    hasPee: boolean;
    notes: string;
    activity: string;
    createdAt: string;
    updatedAt: string;

    constructor(changingObject: any) {
        this.id = changingObject.id;
        this.hasPoo = changingObject.hasPoo;
        this.hasPee = changingObject.hasPee;
        this.notes = changingObject.notes;
        this.activity = changingObject.activity || 'changing';
        this.createdAt = changingObject.createdAt;
        this.updatedAt = changingObject.updatedAt;
    }

    stringify(this: Changing): string {
        let stringified: string;

        if (!this.hasPoo && !this.hasPee) {
            stringified = 'Clean diaper!';
        } else if (!this.hasPoo && this.hasPee) {
            stringified = 'Just pee.';
        } else if (this.hasPoo && !this.hasPee) {
            stringified = 'Just poo.';
        } else if (this.hasPoo && this.hasPee){
            stringified = 'Pee & poo.';
        }

        return stringified;
    }
}
