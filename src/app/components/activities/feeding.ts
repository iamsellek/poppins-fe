export class Feeding {
  id: number;
  isFormula: boolean;
  isBreastmilk: boolean;
  isFood: boolean;
  measurement: string;
  notes: string;
  food: any;
  activity: string;
  createdAt: string;
  updatedAt: string;

  constructor(feedingObject: any) {
    this.id = feedingObject.id;
    this.isFormula = feedingObject.isFormula;
    this.isBreastmilk = feedingObject.isBreastmilk;
    this.isFood = feedingObject.isFood;
    this.measurement = feedingObject.measurement;
    this.notes = feedingObject.notes;
    this.food = feedingObject.food;
    this.activity = feedingObject.activity || 'feeding';
    this.createdAt = feedingObject.createdAt;
    this.updatedAt = feedingObject.updatedAt;
  }

  stringify(this: Feeding): string {
    let stringified: string;

    if (this.isFormula) {
      stringified = ' of formula.';
    } else if (this.isBreastmilk){
      stringified = ' of breastmilk.';
    } else {
      stringified = ` of ${this.food}.`
    }

    return this.measurement + stringified;
  }
}
