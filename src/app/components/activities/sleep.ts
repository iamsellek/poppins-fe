import * as moment from 'moment';

export class Sleep {
  id: number;
  lengthInMinutes: number;
  notes: string;
  activity: string;
  createdAt: string;
  updatedAt: string;

  constructor(sleepObject: any) {
    this.id = sleepObject.id;
    this.lengthInMinutes = sleepObject.lengthInMinutes;
    this.notes = sleepObject.notes;
    this.activity = sleepObject.activity || 'sleep';
    this.createdAt = sleepObject.createdAt;
    this.updatedAt = sleepObject.updatedAt;
  }

  public static findLengthInMinutes(hours, minutes) {
    return (hours * 60) + minutes;
  }

  stringify(this: Sleep): string {
    let stringify: string;
    let hoursInt: number = Math.floor(moment.duration(this.lengthInMinutes * 60 * 1000).asHours());
    let hoursString: string = (hoursInt === 0 || hoursInt > 1) ? 'hours' : 'hour';
    let minInt: number = this.lengthInMinutes - (hoursInt * 60);
    let minString: string = (minInt === 0 || minInt > 1) ? 'minutes' : 'minute';

    stringify = 'Slept for ';

    if (hoursInt > 0) {
      stringify += hoursInt + ' ' + hoursString

      if (minInt > 0) {
          stringify += ' and ';
      } else {
          stringify += '.';
      }
    }

    if (minInt > 0) {
      stringify += minInt + ' ' + minString + '.';
    }

    return stringify;
  }
}
