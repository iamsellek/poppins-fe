export class Milking {
  id: number;
  isLeftBreast: boolean;
  isRightBreast: boolean;
  measurement: string;
  isBreastFeeding: boolean;
  isPumping: boolean;
  notes: string;
  activity: string;
  createdAt: string;
  updatedAt: string;

  constructor(milkingObject: any) {
    this.id = milkingObject.id;
    this.isLeftBreast = milkingObject.isLeftBreast;
    this.isRightBreast = milkingObject.isRightBreast;
    this.measurement = milkingObject.measurement;
    this.isBreastFeeding = milkingObject.isBreastFeeding;
    this.isPumping = milkingObject.isPumping;
    this.notes = milkingObject.notes;
    this.activity = milkingObject.activity || 'milking';
    this.createdAt = milkingObject.createdAt;
    this.updatedAt = milkingObject.updatedAt;
  }

  stringify(this: Milking): string {
    let stringify: string;

    if (this.isPumping) {
      stringify = 'Pumped';
    } else {
      stringify = 'Breastfed';
    }

    stringify += ` ${this.measurement} from`;

    if (this.isLeftBreast) {
      stringify += ' right.'
    } else {
      stringify += ' left.'
    }

    return stringify;
  }
}
