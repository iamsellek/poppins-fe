import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';

import { Changing } from '../activities/changing';
import { Feeding } from '../activities/feeding';
import { Milking } from '../activities/milking';
import { Sleep } from '../activities/sleep';

import { NewActivityData } from '../newActivities/helpers/newActivityData.service'
import { StringConcat } from '../helpers/stringConcat.service';
import { Translate } from '../helpers/translate.service';

@Component({
  selector: 'activity-card',
  styleUrls: [],
  templateUrl: './activityCard.html'
})
export class ActivityCard {
  @Input() recentActivities: any;
  @Input() activityType: string;

  activities = [];

  constructor(private router: Router,
              private newActivityData: NewActivityData,
              private stringConcat: StringConcat,
              private translate: Translate) { }

  ngOnInit() {
    for (let activity of this.recentActivities) {
      if (this.activityType === 'changings') {
        this.activities.push(new Changing(activity));
      } else if (this.activityType === 'feedings') {
        this.activities.push(new Feeding(activity));
      } else if (this.activityType === 'milkings') {
        this.activities.push(new Milking(activity));
      } else if (this.activityType === 'sleeps') {
        this.activities.push(new Sleep(activity));
      }
    }
  }

  newActivity() {
    this.newActivityData.setMostRecentActivity(this.activities[0]);
    this.router.navigate([`/${this.activityType}/new`]);
  }

  goToHistory() {
    this.router.navigate([`/${this.activityType}/history`]);
  }

  titleify(string) {
    string = this.translate.translateActivityType(string);

    return this.stringConcat.capitalizeFirstLetter(string);
  }

  translateActivityType(string) {
    return this.translate.translateActivityType(string);
  }

  depluralize(string) {
      return this.stringConcat.depluralize(string);
  }
}
