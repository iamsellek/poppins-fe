import { Component, OnInit, EventEmitter, ViewContainerRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { Milking } from '../../activities/milking';
import { NewActivityData } from '../helpers/newActivityData.service';
import { NewActivity } from '../helpers/newActivity.service';
import { NewActivityAdded } from '../helpers/newActivityAdded.service';

import { ToastsManager } from 'ng2-toastr/ng2-toastr';

@Component({
  selector: 'new-milking',
  styleUrls: [],
  templateUrl: './newMilking.html'
})

export class NewMilking {
  mostRecentMilking: Milking;
  formGroup: FormGroup;

  unitsOfMeasure = ['oz', 'minutes', 'mL'];
  amount: number;
  selectedUnitOfMeasure: string = 'oz';

  milkingTypes = ['Breastfeeding', 'Pumping'];
  selectedMilkingType: string = 'Breastfeeding';

  breasts = ['Left', 'Right'];
  selectedBreast: string = 'Left';

  notes: string = '';

  constructor(private newActivityData: NewActivityData,
              private newActivityAdded: NewActivityAdded,
              private formBuilder: FormBuilder,
              private newActivity: NewActivity,
              private router: Router,
              private toastr: ToastsManager,
              private vCR: ViewContainerRef)
  {
    this.formGroup = formBuilder.group({
      'selectedMilkingType': [],
      'selectedBreast': [],
      'amount': ['', Validators.required],
      'selectedUnitOfMeasure': [],
      'notes': []
    });

    this.toastr.setRootViewContainerRef(vCR);
  }

  ngOnInit() {
    this.mostRecentMilking = this.newActivityData.getMostRecentActivity();
  }

  onSubmit(form: any) {
    let milking = new Milking({
      isLeftBreast: this.selectedBreast === 'Left',
      isRightBreast: this.selectedBreast === 'Right',
      measurement: `${this.amount} ${this.selectedUnitOfMeasure}`,
      isBreastFeeding: this.selectedMilkingType === 'Breastfeeding',
      isPumping: this.selectedMilkingType === 'Pumping',
      notes: this.notes
    });

    this.newActivity.postNewActivity(milking).then(response => {
      this.newActivityAdded.setActivityType(response.activity);
      this.router.navigate(['poppins']);
    }, error => {
      this.toastr.error('There was an issue.', error);
    });
  }
}
