import { NgModule } from '@angular/core';

import { NewMilking } from './newMilking.component';

@NgModule({
  declarations: [
    NewMilking
  ],
  imports: [],
  providers: [],
  bootstrap: [NewMilking]
})

export class NewMilkingModule { }
