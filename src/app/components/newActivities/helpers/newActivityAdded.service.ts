import { Injectable } from '@angular/core';

@Injectable()
export class NewActivityAdded {
  activityType: string = '';

  setActivityType(type) {
    this.activityType = type;
  }

  getActivityType() {
    let tempType = JSON.parse(JSON.stringify(this.activityType));
    this.resetActivityType();

    return tempType;
  }

  private resetActivityType() {
    this.activityType = '';
  }
}
