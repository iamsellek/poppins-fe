import { Injectable } from '@angular/core';

@Injectable()
export class NewActivityData {
  mostRecentActivity: any;

  setMostRecentActivity(activity) {
      this.mostRecentActivity = activity;
  }

  getMostRecentActivity() {
    return this.mostRecentActivity;
  }
}
