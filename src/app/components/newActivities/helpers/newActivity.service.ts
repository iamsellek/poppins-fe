import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class NewActivity {
  private url = 'http://localhost:4200/api/';

  constructor(private http: Http) { }

  postNewActivity(activity): Promise<any> {
    let tempUrl = `${this.url + activity.activity}s`;

    let object = activity;

    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    let body = JSON.stringify(object);

    return this.http.post(tempUrl, body, options)
                    .toPromise()
                    .then(this.extractToJson)
                    .catch(this.handleError);
  }



  private extractToJson(response: Response) {
        let body = response.json();
        return body || {};
  }

  private handleError(error: any): Promise<any> {
    return Promise.reject(error.full_messages || error.status || error);
  }
}
