import { NgModule } from '@angular/core';

import { NewChanging } from './newChanging.component';

@NgModule({
  declarations: [
    NewChanging
  ],
  imports: [],
  providers: [],
  bootstrap: [NewChanging]
})

export class NewChangingModule { }
