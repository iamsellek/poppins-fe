import { Component, OnInit, EventEmitter, ViewContainerRef } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

import { Changing } from '../../activities/changing';
import { NewActivityData } from '../helpers/newActivityData.service';
import { NewActivity } from '../helpers/newActivity.service';
import { NewActivityAdded } from '../helpers/newActivityAdded.service';

import { ToastsManager } from 'ng2-toastr/ng2-toastr';

@Component({
  selector: 'new-changing',
  styleUrls: [],
  templateUrl: './newChanging.html'
})

export class NewChanging {
  mostRecentChanging: Changing;
  formGroup: FormGroup;
  hasPoo: boolean = false;
  hasPee: boolean = false;
  notes: string = '';

  constructor(private newActivityData: NewActivityData,
              private newActivityAdded: NewActivityAdded,
              private formBuilder: FormBuilder,
              private newActivity: NewActivity,
              private router: Router,
              private toastr: ToastsManager,
              private vCR: ViewContainerRef)
  {
    this.formGroup = formBuilder.group({
      'hasPoo': [],
      'hasPee': [],
      'notes': []
    });

    this.toastr.setRootViewContainerRef(vCR);
  }

  ngOnInit() {
    this.mostRecentChanging = this.newActivityData.getMostRecentActivity();
  }

  onSubmit(form: any) {
    let changing = new Changing({
      hasPoo: this.hasPoo,
      hasPee: this.hasPee,
      notes: this.notes
    });

    this.newActivity.postNewActivity(changing).then(response => {
      this.newActivityAdded.setActivityType(response.activity);
      this.router.navigate(['poppins']);
    }, error => {
      this.toastr.error('There was an issue.', 'Please try again later.');
    });
  }
}
