import { Component, OnInit, EventEmitter, ViewContainerRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { Sleep } from '../../activities/sleep';
import { NewActivityData } from '../helpers/newActivityData.service';
import { NewActivity } from '../helpers/newActivity.service';
import { NewActivityAdded } from '../helpers/newActivityAdded.service';

import { ToastsManager } from 'ng2-toastr/ng2-toastr';

@Component({
  selector: 'new-sleep',
  styleUrls: [],
  templateUrl: './newSleep.html'
})

export class NewSleep {
  mostRecentSleep: Sleep;
  formGroup: FormGroup;

  hours: number;
  minutes: number;

  notes: string = '';

  constructor(private newActivityData: NewActivityData,
              private newActivityAdded: NewActivityAdded,
              private formBuilder: FormBuilder,
              private newActivity: NewActivity,
              private router: Router,
              private toastr: ToastsManager,
              private vCR: ViewContainerRef)
  {
    this.formGroup = formBuilder.group({
      'hours': [],
      'minutes': [],
      'notes': []
    });

    this.toastr.setRootViewContainerRef(vCR);
  }

  ngOnInit() {
    this.mostRecentSleep = this.newActivityData.getMostRecentActivity();
  }

  onSubmit(form: any) {
    console.log(Sleep.findLengthInMinutes(this.hours, this.minutes));
    let sleep = new Sleep({
      lengthInMinutes: Sleep.findLengthInMinutes(this.hours, this.minutes),
      notes: this.notes
    });
    console.log(sleep);
    this.newActivity.postNewActivity(sleep).then(response => {
      this.newActivityAdded.setActivityType(response.activity);
      this.router.navigate(['poppins']);
    }, error => {
      this.toastr.error('There was an issue.', error);
    });
  }
}
