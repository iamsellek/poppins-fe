import { NgModule } from '@angular/core';

import { NewSleep } from './newSleep.component';

@NgModule({
  declarations: [
    NewSleep
  ],
  imports: [],
  providers: [],
  bootstrap: [NewSleep]
})

export class NewSleepModule { }
