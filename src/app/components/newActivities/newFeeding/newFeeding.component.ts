import { Component, OnInit, EventEmitter, ViewContainerRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { Feeding } from '../../activities/feeding';
import { NewActivityData } from '../helpers/newActivityData.service';
import { NewActivity } from '../helpers/newActivity.service';
import { NewActivityAdded } from '../helpers/newActivityAdded.service';

import { ToastsManager } from 'ng2-toastr/ng2-toastr';

@Component({
  selector: 'new-feeding',
  styleUrls: [],
  templateUrl: './newFeeding.html'
})

export class NewFeeding {
  mostRecentFeeding: Feeding;
  formGroup: FormGroup;

  unitsOfMeasure = ['oz', 'mL'];
  amount: number;
  selectedUnitOfMeasure: string = 'oz';

  foodTypes = ['Breastmilk', 'Formula'];
  selectedFoodType: string = 'Breastmilk';

  notes: string = '';

  constructor(private newActivityData: NewActivityData,
              private newActivityAdded: NewActivityAdded,
              private formBuilder: FormBuilder,
              private newActivity: NewActivity,
              private router: Router,
              private toastr: ToastsManager,
              private vCR: ViewContainerRef)
  {
    this.formGroup = formBuilder.group({
      'selectedFoodType': [],
      'amount': ['', Validators.required],
      'selectedUnitOfMeasure': [],
      'notes': []
    });

    this.toastr.setRootViewContainerRef(vCR);
  }

  ngOnInit() {
    this.mostRecentFeeding = this.newActivityData.getMostRecentActivity();
  }

  onSubmit(form: any) {
    let feeding = new Feeding({
      isFormula: this.selectedFoodType === 'Formula',
      isBreastmilk: this.selectedFoodType === 'Breastmilk',
      isFood: this.selectedFoodType === 'Food',
      measurement: `${this.amount} ${this.selectedUnitOfMeasure}`,
      notes: this.notes
    });

    this.newActivity.postNewActivity(feeding).then(response => {
      this.newActivityAdded.setActivityType(response.activity);
      this.router.navigate(['poppins']);
    }, error => {
      this.toastr.error('There was an issue.', error);
    });
  }
}
