import { NgModule } from '@angular/core';

import { NewFeeding } from './newFeeding.component';

@NgModule({
  declarations: [
    NewFeeding
  ],
  imports: [],
  providers: [],
  bootstrap: [NewFeeding]
})

export class NewFeedingModule { }
