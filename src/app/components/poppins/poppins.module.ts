import { NgModule } from '@angular/core';

import { PoppinsComponent } from './poppins.component';

@NgModule({
  declarations: [
    PoppinsComponent
  ],
  imports: [],
  providers: [],
  bootstrap: [PoppinsComponent]
})

export class PoppinsModule { }
