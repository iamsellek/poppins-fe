export class RecentActivities {
  changings: any;
  feedings: any;
  milkings: any;
  sleeps: any;

  constructor(recentActivities: any) {
    this.changings = recentActivities.changings;
    this.feedings = recentActivities.feedings;
    this.milkings = recentActivities.milkings;
    this.sleeps = recentActivities.sleeps;
  }
}
