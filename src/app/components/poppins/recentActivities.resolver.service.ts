import { Injectable } from '@angular/core';
import { Router, Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';

import { PoppinsService } from './poppins.service';
import { RecentActivities } from './recentActivities';

@Injectable()
export class RecentActivitiesResolver implements Resolve<RecentActivities> {
  constructor(private poppinsService: PoppinsService, private router: Router) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<RecentActivities> {
    return this.poppinsService.getRecentActivities().then(recentActivities => {
      return recentActivities;
    });
  }
}
