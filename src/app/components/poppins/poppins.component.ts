import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { RecentActivities } from './recentActivities';
import { NewActivityAdded } from '../newActivities/helpers/newActivityAdded.service';
import { StringConcat } from '../helpers/stringConcat.service';

import { ToastsManager } from 'ng2-toastr/ng2-toastr';

@Component({
  selector: 'poppins',
  styleUrls: [],
  templateUrl: './poppins.html'
})
export class PoppinsComponent implements OnInit {
  recentActivities: RecentActivities;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private newActivityAdded: NewActivityAdded,
              private stringConcat: StringConcat,
              private toastr: ToastsManager,
              private vCR: ViewContainerRef)
  {
    this.toastr.setRootViewContainerRef(vCR);
  }

  ngOnInit() {
    this.route.data.subscribe((data: { recentActivities: RecentActivities }) => {
      this.recentActivities = data.recentActivities;
      let activityType = this.newActivityAdded.getActivityType();

      if (activityType !== '') {
        this.toastr.success(`${this.stringConcat.capitalizeFirstLetter(activityType)} added!`)
      }
    });
  }
}
