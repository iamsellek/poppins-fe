import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { RecentActivities } from './recentActivities';

@Injectable()
export class PoppinsService {
  recentActivities: RecentActivities;

  private url = 'http://localhost:4200/api/activities';

  constructor(private http: Http) { }

  getRecentActivities(): Promise<RecentActivities> {
    return this.http.get(this.url)
               .toPromise()
               .then(response => response.json() as RecentActivities)
               .catch(this.handleError);
  }



  private handleError(error: any): Promise<any> {
    console.error(error);

    return Promise.reject(error.message || error);
  }
}
