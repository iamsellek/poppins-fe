import { NgModule } from '@angular/core';

import { AllSleeps } from './allSleeps.component';

@NgModule({
  declarations: [
    AllSleeps
  ],
  imports: [],
  providers: [],
  bootstrap: [AllSleeps]
})

export class AllSleepsModule { }
