import { Component } from '@angular/core';

import { Sleep } from '../../activities/sleep';

import { ActivityHistory } from '../helpers/activityHistory.service';

@Component({
  selector: 'all-sleeps',
  styleUrls: [],
  templateUrl: './allSleeps.html'
})

export class AllSleeps {
  sleeps: any = [];

  constructor(private activityHistory: ActivityHistory) {}

  ngOnInit() {
    this.activityHistory.getActivities('sleeps').then(response => {
      for (let sleep of response) {
        this.sleeps.push(new Sleep(sleep));
      }
    });
  }

  goHome() {
    this.activityHistory.goHome();
  }
}
