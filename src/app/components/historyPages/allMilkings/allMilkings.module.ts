import { NgModule } from '@angular/core';

import { AllMilkings } from './allMilkings.component';

@NgModule({
  declarations: [
    AllMilkings
  ],
  imports: [],
  providers: [],
  bootstrap: [AllMilkings]
})

export class AllMilkingsModule { }
