import { Component } from '@angular/core';

import { Milking } from '../../activities/milking';

import { ActivityHistory } from '../helpers/activityHistory.service';

@Component({
  selector: 'all-milkings',
  styleUrls: [],
  templateUrl: './allMilkings.html'
})

export class AllMilkings {
  milkings: any = [];

  constructor(private activityHistory: ActivityHistory) {}

  ngOnInit() {
    this.activityHistory.getActivities('milkings').then(response => {
      for (let milking of response) {
        this.milkings.push(new Milking(milking));
      }
    });
  }

  goHome() {
    this.activityHistory.goHome();
  }
}
