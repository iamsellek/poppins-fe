import { NgModule } from '@angular/core';

import { AllChangings } from './allChangings.component';

@NgModule({
  declarations: [
    AllChangings
  ],
  imports: [],
  providers: [],
  bootstrap: [AllChangings]
})

export class AllChangingsModule { }
