import { Component } from '@angular/core';

import { Changing } from '../../activities/changing';

import { ActivityHistory } from '../helpers/activityHistory.service';

@Component({
  selector: 'all-changings',
  styleUrls: [],
  templateUrl: './allChangings.html'
})

export class AllChangings {
  changings: any = [];

  constructor(private activityHistory: ActivityHistory) {}

  ngOnInit() {
    this.activityHistory.getActivities('changings').then(response => {
      for (let changing of response) {
        this.changings.push(new Changing(changing));
      }
    });
  }

  goHome() {
    this.activityHistory.goHome();
  }
}
