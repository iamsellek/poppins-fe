import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Router } from '@angular/router';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class ActivityHistory {
  private url = 'http://localhost:4200/api/';

  constructor(private http: Http, private router: Router) { }

  getActivities(activity): Promise<any> {
    let tempUrl = `${this.url}${activity}`

    return this.http.get(tempUrl)
               .toPromise()
               .then(response => response.json() as any)
               .catch(this.handleError);
   }

   goHome() {
     this.router.navigate(['poppins']);
   }



  private extractToJson(response: Response) {
        let body = response.json();
        return body || {};
  }

  private handleError(error: any): Promise<any> {
    return Promise.reject(error.full_messages || error.status || error);
  }
}
