import { NgModule } from '@angular/core';

import { AllFeedings } from './allFeedings.component';

@NgModule({
  declarations: [
    AllFeedings
  ],
  imports: [],
  providers: [],
  bootstrap: [AllFeedings]
})

export class AllFeedingsModule { }
