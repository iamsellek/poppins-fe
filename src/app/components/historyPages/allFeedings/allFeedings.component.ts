import { Component } from '@angular/core';

import { Feeding } from '../../activities/feeding';

import { ActivityHistory } from '../helpers/activityHistory.service';

@Component({
  selector: 'all-feedings',
  styleUrls: [],
  templateUrl: './allFeedings.html'
})

export class AllFeedings {
  feedings: any = [];

  constructor(private activityHistory: ActivityHistory) {}

  ngOnInit() {
    this.activityHistory.getActivities('feedings').then(response => {
      for (let feeding of response) {
        this.feedings.push(new Feeding(feeding));
      }
    });
  }

  goHome() {
    this.activityHistory.goHome();
  }
}
