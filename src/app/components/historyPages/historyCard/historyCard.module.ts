import { NgModule } from '@angular/core';

import { HistoryCard } from './historyCard.component';

@NgModule({
  declarations: [
    HistoryCard
  ],
  imports: [],
  providers: [],
  bootstrap: [HistoryCard]
})

export class HistoryCardModule { }
