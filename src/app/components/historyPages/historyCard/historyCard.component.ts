import { Component, Input } from '@angular/core';

@Component({
  selector: 'history-card',
  styleUrls: [],
  templateUrl: './historyCard.html'
})

export class HistoryCard {
  @Input() activity: any;
}
