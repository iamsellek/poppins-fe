import { NgModule } from '@angular/core';

import { LastActivity } from './lastActivity.component';

@NgModule({
  declarations: [
    LastActivity
  ],
  imports: [],
  providers: [],
  bootstrap: [LastActivity]
})

export class LastActivityModule { }
