import { Component, Input } from '@angular/core';

import { Translate } from '../helpers/translate.service';

@Component({
  selector: 'last-activity',
  styleUrls: [],
  templateUrl: './lastActivity.html'
})

export class LastActivity {
  @Input() lastActivity: any;

  constructor(private translate: Translate) {}

  translateActivityType(string) {
    return this.translate.translateActivityType(string);
  }
}
