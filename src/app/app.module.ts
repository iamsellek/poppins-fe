import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { RecentActivitiesResolver } from './components/poppins/recentActivities.resolver.service';
import { PoppinsComponent } from './components/poppins/poppins.component';
import { ActivityCard } from './components/activityCard/activityCard.component';
import { NewChanging } from './components/newActivities/newChanging/newChanging.component';
import { AllChangings } from './components/historyPages/allChangings/allChangings.component';
import { NewFeeding } from './components/newActivities/newFeeding/newFeeding.component';
import { AllFeedings } from './components/historyPages/allFeedings/allFeedings.component';
import { NewMilking } from './components/newActivities/newMilking/newMilking.component';
import { AllMilkings } from './components/historyPages/allMilkings/allMilkings.component';
import { NewSleep } from './components/newActivities/newSleep/newSleep.component';
import { AllSleeps } from './components/historyPages/allSleeps/allSleeps.component';
import { LastActivity } from './components/lastActivity/lastActivity.component';
import { HistoryCard } from './components/historyPages/historyCard/historyCard.component';

import { PoppinsService } from './components/poppins/poppins.service';
import { NewActivityData } from './components/newActivities/helpers/newActivityData.service';
import { NewActivity } from './components/newActivities/helpers/newActivity.service';
import { NewActivityAdded } from './components/newActivities/helpers/newActivityAdded.service';
import { StringConcat } from './components/helpers/stringConcat.service';
import { Translate } from './components/helpers/translate.service';
import { ActivityHistory } from './components/historyPages/helpers/activityHistory.service';

import { MaterializeModule } from "angular2-materialize";
import { ToastModule } from 'ng2-toastr/ng2-toastr';

const appRoutes: Routes = [
  {
    path: '',
    redirectTo: 'poppins',
    pathMatch: 'full'
  },
  {
    path: 'poppins',
    component: PoppinsComponent,
    resolve: { recentActivities: RecentActivitiesResolver },
    data: { preload: true }
  },
  {
    path: 'changings/new',
    component: NewChanging
  },
  {
    path: 'changings/history',
    component: AllChangings
  },
  {
    path: 'feedings/new',
    component: NewFeeding
  },
  {
    path: 'feedings/history',
    component: AllFeedings
  },
  {
    path: 'milkings/new',
    component: NewMilking
  },
  {
    path: 'milkings/history',
    component: AllMilkings
  },
  {
    path: 'sleeps/new',
    component: NewSleep
  },
  {
    path: 'sleeps/history',
    component: AllSleeps
  }
];

@NgModule({
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    HttpModule,
    FormsModule,
    MaterializeModule,
    ReactiveFormsModule,
    RouterModule.forRoot(appRoutes),
    ToastModule.forRoot()
  ],
  declarations: [
    AppComponent,
    PoppinsComponent,
    ActivityCard,
    LastActivity,
    NewChanging,
    AllChangings,
    NewFeeding,
    AllFeedings,
    NewMilking,
    AllMilkings,
    NewSleep,
    AllSleeps,
    HistoryCard
  ],
  providers: [
    NewActivity,
    NewActivityAdded,
    NewActivityData,
    PoppinsService,
    RecentActivitiesResolver,
    StringConcat,
    Translate,
    ActivityHistory
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
