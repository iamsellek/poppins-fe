import { Component } from '@angular/core';

@Component({
  selector: 'my-app',
  styleUrls: [],
  template: '<router-outlet></router-outlet>'
})

export class AppComponent {
  title = 'Poppins';
}
